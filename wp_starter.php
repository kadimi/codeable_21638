<?php
/*
Plugin Name: Codeable 21638
Plugin URI: http://www.kadimi.com/
Description: Codeable 21638
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

// Include Skelet (written by Nabil Kadimi)
include dirname( __FILE__ ) . '/skelet/skelet.php';

// Include options definitions
skelet_dir( dirname( __FILE__ ) . '/data' );

// Use ACF Lite
include plugin_dir_path( __FILE__ ) . 'inc/plugins/advanced-custom-fields/acf.php';
define( 'ACF_LITE', true );

// Change 'Finish Quiz text'
add_filter( 'gettext', 'ca21638_on_gettext', 10, 3 );
function ca21638_on_gettext( $translated_text, $text, $domain ) {

	switch ( $translated_text ) {
	case 'Start quiz' :
	case 'Restart quiz' :
	case 'Finish quiz' :
		$post_id = get_the_ID();
		$txt = get_post_meta( $post_id, 'quiz_txt', true );
		if ( ! $txt )
			$txt = 'quiz';
		$translated_text = str_replace( 'quiz', $txt, $translated_text );
		break;
	}

	return $translated_text;
}

/**
 * Initialize plugin
 *
 * - Define more useful constants
 * - Register field group
 */
add_action( 'init', 'ca21638_on_init' );
function ca21638_on_init() {
	
	register_field_group( array (
		'id' => 'acf_quiz-txt',
		'title' => __( 'Additional Quiz Options' ),
		'fields' => array (
			array (
				'key' => 'field_quiz_txt',
				'label' => __( 'Alternative text for "quiz" in "Start/Restart/Finish quiz"' ),
				'name' => 'quiz_txt',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'e.g. assessment',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-quiz',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	) );
}

/**
 * Add "quiz" or its replacement to body classes
 */
add_filter('body_class', 'ca21638_on_body_classes');
function ca21638_on_body_classes( $classes ) {
	$post_id = get_the_ID();
	$post = get_post( $post_id );
	if ( 'sfwd-quiz' === $post->post_type ) {
		$txt = get_post_meta( $post_id, 'quiz_txt', true );
		if ( ! $txt )
			$txt = 'quiz';
		$classes[] = $txt;
	}
	return $classes;
}