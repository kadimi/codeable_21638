<?php

// Register shortcodes
paf_shortcodes( array(
	'mark_complete' => array(
		'title' => __( 'Mark Compete' ),
		'image' => '//png.findicons.com/files/icons/2198/dark_glass/128/camera_test.png',
		'menu'  => true,
	),
	'complete_lesson' => array(
		'parent'     => 'mark_complete',
		'height'     => '.8',
		'text'       => __( 'Complete Lesson' ),
		'title'      => __( 'Mark Lesson Complete' ),
		'parameters' => array(
			'id' => array(
				'title'   => 'Lesson',
				'type'    => 'select',
				'options' => 'posts',
				'args'    => array(
					'post_type'   => 'sfwd-lessons',
					'post_status' => 'publish',
				),
			),
			'text' => array(
				'title' => 'Text for "Complete Lesson"',
				'value' => 'Complete Lesson',
			),
			// 'hide_if_competed' => array(
			// 	'title' => 'Hide if Completed',
			// 	'type' => 'radio',
			// 	'options' => array(
			// 		'0' => 'No',
			// 		'1' => 'Yes',
			// 	),
			// 	'selected' => '1',
			// ),
			// 'text_completed' => array(
			// 	'title' => 'Text for "Lesson Completed"',
			// 	'value' => 'Lesson Completed',
			// 	'conditions' => array(
			// 		array( 'hide_if_competed', 'neq', '1' ),
			// 	),
			// ),
		),
	),
	'complete_course' => array(
		'parent'     => 'mark_complete',
		'height'     => '.8',
		'text'       => __( 'Complete Course' ),
		'title'      => __( 'Mark Course Complete' ),
		'parameters' => array(
			'id' => array(
				'title'   => 'Course',
				'type'    => 'select',
				'options' => 'posts',
				'args'    => array(
					'post_type'   => 'sfwd-courses',
					'post_status' => 'publish',
				),
			),
			'text' => array(
				'title' => 'Text for "Complete Course"',
				'value' => 'Complete Course',
			),
		),
	),
) );

/**
 * Return output for the shortcode "complete_lesson"
 */
function complete_lesson_func( $atts = array(), $content = null ) {

	// Return if missing id
	if ( empty( $atts[ 'id' ] ) ) return '';

	// Get lesson status
	$is_lesson_completed = learndash_lesson_topics_completed( $atts[ 'id' ] );

	// Hide if completed when applicable
	if ( $is_lesson_completed /* && ! empty( $atts[ 'hide_if_competed' ] ) */ ) return '';

	// Template
	$tpl = $is_lesson_completed
		? '<input value="{{text_completed}}" type="submit" disabled>'
		: '<form id="sfwd-mark-complete" method="post" action=""><input value="{{id}}" name="post" type="hidden"><input value="{{text}}" name="sfwd_mark_complete" type="submit"></form>'
	;

	// Interpolate
	$o = str_replace( array( '{{id}}', '{{text}}', '{{text_completed}}' )
		, array( $atts[ 'id' ], $atts[ 'text' ], $atts[ 'text_completed' ] )
		, $tpl
	);

	// Return shortcode output
	return $o;
}

/**
 * Return output for the shortcode "complete_lesson"
 */
function complete_course_func( $atts = array(), $content = null ) {

	// Return if missing id
	if ( empty( $atts[ 'id' ] ) ) return '';

	// Get course status
	$progress = learndash_get_course_progress( null, $atts[ 'id' ] );
	$is_course_completed = ! empty( $progress[ 'this' ]->completed );

	// Hide if completed when applicable
	if ( $is_course_completed ) return '';

	// Template
	$tpl = '<form id="sfwd-mark-complete" method="post" action=""><input value="{{id}}" name="post" type="hidden"><input value="{{text}}" name="sfwd_mark_complete" type="submit"></form>';

	// Interpolate
	$o = str_replace( array( '{{id}}', '{{text}}', '{{text_completed}}' )
		, array( $atts[ 'id' ], $atts[ 'text' ], $atts[ 'text_completed' ] )
		, $tpl
	);

	// Return shortcode output
	return $o;
}
